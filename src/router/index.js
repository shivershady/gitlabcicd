import VueRouter from "vue-router";
import Vue from "vue";

Vue.use(VueRouter);

export const router = new VueRouter({
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import('@/pages/Home.vue'),
        },
        {
            path: '/categories',
            name: 'categories',
            component: () => import('@/pages/Category.vue'),
        },
        {
            path: '/article',
            name: 'article',
            component: () => import('@/pages/Article.vue'),
        },
    ],
    mode: "history",

});