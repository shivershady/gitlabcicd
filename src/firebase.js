import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyDq05NmSFtoReYq0C5hiiyh5vgYOWGQ8YI",
    authDomain: "article-react.firebaseapp.com",
    projectId: "article-react",
    storageBucket: "article-react.appspot.com",
    messagingSenderId: "196999653591",
    appId: "1:196999653591:web:a501952c833093a717e892"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// firestore database // luu tru CSDL
const db = getFirestore(app);
const auth = getAuth(app);
export { db, auth };